// CRUD Operations:
/*
	1. Create - insert
	2. Read - find
	3. Update - update
	4. Destroy - delete
*/


// CREATE OR INSERT METHOD () - Create documents in our database (DB)

/*
	Syntax:
		Insert One Document:
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			})

		Insert Many Documents:
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}
			])
*/

// REMINDER: Upon execution on Robo 3T MAKE SURE NOT TO EXECUTE your data several times as it will duplicate


//SAMPLE

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Dela Cruz",
	"age": 21,
	"email": "janedc@ymail.com",
	"company": "none"
});


db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Hawkings",
		"age": 67,
		"email": "stephenhawkings@gmail.com",
		"department": "none"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"email": "neilarmstrong@gmail.com",
		"department": "none"
	}
]);


//Mini Activity
	/*
		1. Make a new collection with the name "courses"
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS
			isActive: true
	*/

db.courses.insertMany([
		{
			"name": "Javascript 101",
			"price": 2000,
			"description": "Introduction to Javascript",
			"isActive": true
		},
		{
			"name": "HTML 101",
			"price": 5000,
			"description": "Introduction to HTML",
			"isActive": true
		},
		{
			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": true
		}
	]);


//Read or Find Method - Reads data which matches the data saved in the database.
/*
	Syntax:
		db.collectionName.find() - this will retrieve all the documents from our DB.

		db.collectionName.find({"criteria": "value"}) - this will retrieve all the documents that will match our criteria

		db.collectionName.findOne({"critreria": "value"}) - this will return the 1st document in our collection that match our criteria

		db.collectionName.findOne({}) - this will return the 1st document in our collection 
*/

db.users.find();

db.users.find({
	"firstName": "Jane"
});

db.users.find({
	"firstName": "Neil",
	"age": 82
});


//Update Method () - Updates our documents in our collection
/*

	updateOne() - updating the 1st matching document on our collection
	Syntax: 
		db.collectionName.updateOne({
			"criteria": "value"
		},
		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}
		});

	
	updateMany() - it updates all the documents that matches our criteria
	
	Syntax:
		db.collectionName.updateMany({
			"criteria": "value"
		},
		{
			$set: {
				"fieldToBeUpdated": "updatedValue"
			}
		})

*/

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@mail.com",
	"department": "none"
});

//Updating one document

db.users.updateOne(
		{
			"firstName": "Test"
		},
		{
			$set: {
				"firstName": "Bill",
				"lastName": "Gates",
				"age": 65,
				"email": "billgates@gmail.com",
				"department": "operations",
				"status": "active"
			}
		}
	);


//Removing a field using update method

db.users.updateOne(
		{
			"firstName": "Bill"
		},
		{
			$unset: {
				"status": "active"
			}
		}
	);

//Updating Multiple Documents

db.users.updateMany(
	{
		"department": "none"
	},
	{
		$set: {
			"department": "HR"
		}
	}

);

db.users.updateOne(
		{},
		{
			$set: {
				"department": "Operations"
			}
		}
	);


db.courses.updateOne(
		{
			"name": "HTML 101"
		},
		{
			$set: {
				"isActive": false
			}
		}
	);


db.courses.updateMany(
		{},
		{
			$set: {
				"enrollees": 10
			}
		}
	);


// Destroy / Delete Method () - deleting documents from our collection

db.users.insertOne({


		"firstName":"Test"
	});


//deleting a single document
/*
	Syntax:
	db.collectionName.deletOne({"criteria": "value"})
*/

db.users.deleteOne({
	"firstName": "Test"
});


//Deleting multiple documents
/*
	Syntax:
		db.collectionName.deleteMany({"criteria": "value"})
*/

db.users.deleteMany(
		{
			"department": "HR"
		}
	);


//deleting without criteria

db.courses.deleteMany({});